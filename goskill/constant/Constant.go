package constant

const FILE_DB = "db/fileupload_meta.json"
const LOG_FILE = "log/logs.txt"
const IMAGE_PATH = "image/"
const IMAGE_CONTENT_TYPE = "image/jpeg"
const IMAGE_PNG_CONTENT_TYPE = "image/png"
const MAX_UPLOAD_SIZE = 8 * 1024 * 1024 // 1MB
const SEVER_PORT = ":9090"
const AUTHEN_CODE_ENV_VAR = "HOME"
const UPLOAD_FILE_NAME = "upload.html"
