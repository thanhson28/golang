package main

import (
	"goskill.com/upload/constant"
	"goskill.com/upload/service"
	"log"
	"os"
)

func main() {

	initLog()

	log.Println("Start Upload main()..")
	//os.Setenv("AUTHEN_CODE_ENV_VAR", "son.phan@user")
	//fmt.Println(len(os.Args), os.Args[1])
	service.ENV_AUTHEN_CODE = os.Getenv(constant.AUTHEN_CODE_ENV_VAR)

	log.Println("Setup route and start server..")
	service.SetUpRoute(constant.SEVER_PORT)
}

func initLog() {
	//init log
	// If the file doesn't exist, create it or append to the file
	file, err := os.OpenFile(constant.LOG_FILE, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		log.Fatal(err)
	}
	log.SetOutput(file)
}
