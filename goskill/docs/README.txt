
For using authentication code by environment variable :
 - I am using environment variable as `HOME` which is existing in system .
 If you want to change to another system , please modify to other env variable such as example ${AUTHEN_ENV_VAR}, etc..
  ```
  const AUTHEN_CODE_ENV_VAR = "HOME"
  ```
  ===>
  ```
  const AUTHEN_CODE_ENV_VAR = "AUTHEN_ENV_VAR"
  ```

1. Open source code  in attachment email (it was downloaded from git repository)
    - Project has name `goskill`
    - This project was implemented with  Go SDK 1.18
    - The source code commited in bitbucket ( https://bitbucket.org/thanhson28/golang/src/master/ )

2. cd to goskill/

3. Execute command to build (or using the existing built file in : ./upload)  :
   $ go build upload.go

4. Execute
   $ ./upload

5. Access : http://localhost:9090/view

6. Click to button 'Choose File ' and then click 'Upload Image'

7. The output file :
   - the log file is log/logs.txt
   - db store in file :  db/fileupload_meta.json
   - image stored in : image/
8. If you got problem in click to Download in the list , you can copy link and open new tab , then Past & Go
  (sometime Chrome prevent open local link and show the message `about:blank#blocked` in web browser)



