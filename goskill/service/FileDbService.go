package service

import (
	"encoding/json"
	"fmt"
	"goskill.com/upload/constant"
	"io"
	"io/ioutil"
	"log"
	"math/rand"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

type CollectionFileMeta struct {
	ListFileMetaDatas []FileUploadMeta `json:"file_metas"`
}

type FileUploadMeta struct {
	FileName    string
	ContentType string
	Size        int64
	ModifiedAt  string
	FilePath    string
}

func storeToDb(newFileName string) int {
	absFilePath, err := filepath.Abs(newFileName)
	if err != nil {
		log.Println("Can not find in file system . File name  ", newFileName)
	}
	fileInfo, _ := os.Stat(newFileName)
	newFileUploadMeta := FileUploadMeta{
		FileName:    fileInfo.Name(),
		ContentType: "image/jpeg",
		Size:        fileInfo.Size(),
		ModifiedAt:  fileInfo.ModTime().String(),
		FilePath:    absFilePath,
	}

	//load all existing file metas
	allFileMetas := loadAllFileMetas(constant.FILE_DB)
	allFileMetas.ListFileMetaDatas = append(allFileMetas.ListFileMetaDatas, newFileUploadMeta)

	//write with append data
	appendDataArr, _ := json.MarshalIndent(allFileMetas, "", " ")
	_ = ioutil.WriteFile(constant.FILE_DB, appendDataArr, 0644)

	totalFileMetas := len(allFileMetas.ListFileMetaDatas)
	log.Printf("Adding new file %s. All new item is %s ", newFileName, totalFileMetas)
	return totalFileMetas
}

func loadAllFileMetas(dbStoreFileName string) CollectionFileMeta {
	file, _ := ioutil.ReadFile(dbStoreFileName)
	allFileMetas := CollectionFileMeta{}
	_ = json.Unmarshal([]byte(file), &allFileMetas)
	return allFileMetas
}

func saveImage(uploadFile multipart.File, fileHeader *multipart.FileHeader, writer http.ResponseWriter) (string, bool) {
	// Create file
	newFileName := constant.IMAGE_PATH + fileHeader.Filename
	if fileExists(newFileName) {
		rand.Seed(time.Now().UnixNano())
		splitArrStr := strings.Split(newFileName, ".")
		newFileName = fmt.Sprintf("%s-%s.%s", splitArrStr[0], strconv.Itoa(rand.Int()), splitArrStr[1])
	}

	newFile, err := os.Create(newFileName)
	defer newFile.Close()
	if err != nil {
		http.Error(writer, err.Error(), http.StatusInternalServerError)
		return newFileName, true
	}

	// Copy the uploaded file to the created file on the filesystem
	bytesCopied, err := io.Copy(newFile, uploadFile)
	if err != nil {
		http.Error(writer, err.Error(), http.StatusInternalServerError)
		return newFileName, true
	}
	log.Printf("Copied %d bytes. Image saved succesful!", bytesCopied)
	return newFileName, true
}

func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}
