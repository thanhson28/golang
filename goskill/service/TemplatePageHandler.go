package service

import (
	"goskill.com/upload/constant"
	"html/template"
	"net/http"
)

var templates = template.Must(template.ParseFiles("template/upload.html"))

func returnFobidden(writer http.ResponseWriter, message string) {
	writer.WriteHeader(http.StatusForbidden)
	writer.Write([]byte("403 HTTP status code returned!" + message))
}

func loadPage(message string) *PageInfo {
	return &PageInfo{Title: "Upload Page", AuthenCode: ENV_AUTHEN_CODE, Message: message}
}

func renderTemplate(resWriter http.ResponseWriter, message string, listFileMetas CollectionFileMeta) {
	page := loadPage(message)
	page.ListFileUploads = listFileMetas.ListFileMetaDatas
	if len(listFileMetas.ListFileMetaDatas) > 0 {
		page.HasFileInDb = true
	}
	err := templates.ExecuteTemplate(resWriter, constant.UPLOAD_FILE_NAME, page)
	if err != nil {
		http.Error(resWriter, err.Error(), http.StatusInternalServerError)
	}
}
