package service

import (
	"fmt"
	"goskill.com/upload/constant"
	"log"
	"net/http"
	"strconv"
)

type PageInfo struct {
	Title           string
	AuthenCode      string
	Message         string
	ListFileUploads []FileUploadMeta
	HasFileInDb     bool
}

var ENV_AUTHEN_CODE = ""

func viewHandler(resWriter http.ResponseWriter, r *http.Request) {
	fmt.Println("Start render upload form")
	listOfFileMetas := loadAllFileMetas(constant.FILE_DB)
	allFileMeta := len(listOfFileMetas.ListFileMetaDatas)
	message := "Total upload file current is " + strconv.Itoa(allFileMeta)

	renderTemplate(resWriter, message, listOfFileMetas)
}

func uploadFileHandler(writer http.ResponseWriter, request *http.Request) {
	log.Println("Upload file handler ...")

	//Validate authentication code
	authenCode := request.FormValue("authen")
	if ENV_AUTHEN_CODE != authenCode {
		log.Printf("Invalid authencode. It must be %s but actual is %s \n", ENV_AUTHEN_CODE, authenCode)
		returnFobidden(writer, "Invalid authentication code")
		return
	}

	//validate max size
	request.Body = http.MaxBytesReader(writer, request.Body, 32<<20+1024)
	checkingSizeErr := request.ParseMultipartForm(32<<20 + 1024)
	if checkingSizeErr != nil {
		returnFobidden(writer, "The uploaded file is too big. Please choose an file that's less than 8 MB in size ")
		return
	}

	//Checking file type
	file, fileHeader, error := request.FormFile("uploadFile")
	if error != nil {
		http.Error(writer, error.Error(), http.StatusInternalServerError)
	}
	defer file.Close()

	//validate image file type
	contentType := fileHeader.Header.Get("Content-Type")
	if contentType != constant.IMAGE_CONTENT_TYPE && contentType != constant.IMAGE_PNG_CONTENT_TYPE {
		returnFobidden(writer, "Require upload file must be image/jpeg")
		return
	}

	newFileName, result := saveImage(file, fileHeader, writer)
	if result {
		//store new file into database
		storeToDb(newFileName)

		//return
		http.Redirect(writer, request, "/view", http.StatusFound)
		return
	}

	log.Println("End file handler ...")
	http.Redirect(writer, request, "/view", http.StatusFound)
}

/*
This is the function which handle route the features : <br/>
 <u>
  <li>upload</li>
  <li>view</li>
</u>
*/
func SetUpRoute(serverPort string) {
	//Upload route
	http.HandleFunc("/upload", uploadFileHandler)
	http.HandleFunc("/view", viewHandler)

	//register listen on port
	http.ListenAndServe(serverPort, nil)

}
